// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'yaru22.angular-timeago',  'starter.controllers'])

.run(function($ionicPlatform, timeAgo) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    var oneDay = 60*60*24;
    timeAgo.settings.fullDateAfterSeconds = oneDay;

  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.gatha', {
    url: '/gatha',
    views: {
      'menuContent': {
        templateUrl: 'templates/gatha/gatha.html',
        controller: 'GathaController'
      }
    }
  })

  .state('app.gathaList', {
    url: '/gatha/:categType',
    views: {
      'menuContent': {
        templateUrl: 'templates/gatha/gatha_list.html',
        controller: 'GathaController'
      }
    }
  })

  .state('app.gathaDetail', {
    url: '/gatha/info/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/gatha/gatha_info.html',
        controller: 'GathaController'
      }
    }
  })

  .state('app.pirith', {
      url: '/pirith',
      views: {
        'menuContent': {
          templateUrl: 'templates/pirith/pirith.html',
          controller: 'PirithController'
        }
      }
    })

   .state('app.pirithList', {
    url: '/pirith/:categType',
    views: {
      'menuContent': {
        templateUrl: 'templates/pirith/pirith_list.html',
        controller: 'PirithController'
      }
    }
  })

  .state('app.pirithDetail', {
    url: '/pirith/info/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/pirith/pirith_info.html',
        controller: 'PirithController'
      }
    }
  })

  .state('app.deshana', {
      url: '/deshana',
      views: {
        'menuContent': {
          templateUrl: 'templates/deshana/deshana.html',
          controller: 'DeshanaController'
        }
      }
    })

  .state('app.deshanaList', {
    url: '/deshana/:categType',
    views: {
      'menuContent': {
        templateUrl: 'templates/deshana/deshana_list.html',
        controller: 'DeshanaController'
      }
    }
  })

  .state('app.deshanaDetail', {
    url: '/deshana/info/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/pirith/deshana_info.html',
        controller: 'DeshanaController'
      }
    }
  })

  .state('app.events', {
      url: '/events',
      views: {
        'menuContent': {
          templateUrl: 'templates/events/events.html'
        }
      }
    })

  .state('app.giveBack', {
      url: '/giveBack',
      views: {
        'menuContent': {
          templateUrl: 'templates/giveBack.html'
        }
      }
    })

  .state('app.liveStreams', {
      url: '/liveStreams',
      views: {
        'menuContent': {
          templateUrl: 'templates/liveStreams.html'
        }
      }
    })

  .state('app.aboutUs', {
      url: '/aboutUs',
      views: {
        'menuContent': {
          templateUrl: 'templates/aboutUs.html'
        }
      }
    })


  .state('app.latest', {
      url: '/latest',
      views: {
        'menuContent': {
          templateUrl: 'templates/latest.html'
        }
      }
    });


  // .state('app.single', {
  //   url: '/playlists/:playlistId',
  //   views: {
  //     'menuContent': {
  //       templateUrl: 'templates/playlist.html',
  //       controller: 'PlaylistCtrl'
  //     }
  //   }
  // });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/latest');
});
