angular.module('starter.controllers', [ 'ionic', 'ngCordova', 'starter.services'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})


.controller('GathaController',  function($scope, $stateParams, dharmaService) {

    $scope.timeInMs = Date.now();

    $scope.getCategories = function() {
      $scope.message = "Gatha categories loading...";

       var promise = dharmaService.listCategories('GATHA');
        promise.then(function (data) {
         $scope.categories = data;
         $scope.message = "";
         $scope.$broadcast('scroll.refreshComplete');
        },
        function (errorMessage) {
        // $scope.message = errorMessage + " ERROR";
        });
    }

    $scope.getList = function() {
       $scope.message = "Gatha list loading...";
       var categType = $stateParams.categType;

       var promise = dharmaService.getListByCategory(categType);
        promise.then(function (data) {
         $scope.categories = data;
         $scope.message = "";
         $scope.$broadcast('scroll.refreshComplete');
      },
      function (errorMessage) {
        // $scope.message = errorMessage + " ERROR";
      });
    }

    $scope.getById = function() {
       $scope.message = "Gatha loading...";
       var id = $stateParams.id;
       
       var promise = dharmaService.getInfoById(id);
        promise.then(function (data) {
         $scope.categories = data;
         $scope.message = "";
      },
      function (errorMessage) {
        // $scope.message = errorMessage + " ERROR";
      });
    }

   })

.controller('PirithController',  function($scope, $stateParams, dharmaService) {

    $scope.getCategories = function() {
      $scope.message = "Pirith categories loading...";

       var promise = dharmaService.listCategories('PIRITH');
        promise.then(function (data) {
         $scope.categories = data;
         $scope.message = "";
         $scope.$broadcast('scroll.refreshComplete');
      },
      function (errorMessage) {
        // $scope.message = errorMessage + " ERROR";
      });
    }

    $scope.getList = function() {
       $scope.message = "Pirith list loading...";
       var categType = $stateParams.categType;

       var promise = dharmaService.getListByCategory(categType);
        promise.then(function (data) {
         $scope.categories = data;
         $scope.message = "";
         $scope.$broadcast('scroll.refreshComplete');
        },
        function (errorMessage) {
          // $scope.message = errorMessage + " ERROR";
        });
    }

    $scope.getById = function() {
       $scope.message = "Pirith loading...";
       var id = $stateParams.id;
       
       var promise = dharmaService.getInfoById(id);
        promise.then(function (data) {
         $scope.categories = data;
         $scope.message = "";
      },
      function (errorMessage) {
        // $scope.message = errorMessage + " ERROR";
      });
    }

   })


.controller('DeshanaController',  function($scope, $stateParams, dharmaService) {

    $scope.getCategories = function() {
      $scope.message = "Deshana categories loading...";

       var promise = dharmaService.listCategories('DESHANA');
        promise.then(function (data) {
         $scope.categories = data;
         $scope.message = "";
         $scope.$broadcast('scroll.refreshComplete');
      },
      function (errorMessage) {
        // $scope.message = errorMessage + " ERROR";
      });
    }

    $scope.getList = function() {
       $scope.message = "Deshana list loading...";
       var categType = $stateParams.categType;

       var promise = dharmaService.getListByCategory(categType);
        promise.then(function (data) {
         $scope.categories = data;
         $scope.message = "";
         $scope.$broadcast('scroll.refreshComplete');
      },
      function (errorMessage) {
        // $scope.message = errorMessage + " ERROR";
      });
    }

    $scope.getById = function() {
       $scope.message = "Deshana loading...";
       var id = $stateParams.id;
       
       var promise = dharmaService.getInfoById(id);
        promise.then(function (data) {
         $scope.categories = data;
         $scope.message = "";
      },
      function (errorMessage) {
        // $scope.message = errorMessage + " ERROR";
      });
    }

   })

.controller('LatestItemsController',  function($scope, $stateParams, dharmaService) {

    $scope.search = function() {
       var query = $scope.query;
       var promise = dharmaService.search(query);
        promise.then(function (data) {
         $scope.items = data;
         $scope.message = "";
         $scope.$broadcast('scroll.refreshComplete');
        },
        function (errorMessage) {
        // $scope.message = errorMessage + " ERROR";
        });
    }

   })


.controller('FileTransferController', function($scope,  $timeout, $cordovaFileTransfer) {
         
  $scope.fileDownload = function (url) {

    var url = "http://cdn.wall-pix.net/albums/art-space/00030109.jpg";
    var filename = url.split("/").pop();
    var targetPath = "downloads/"+ filename;
    var trustHosts = true
    var options = {};

    $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
      .then(function(result) {
        // Success!
      }, function(err) {
        // Error
      }, function (progress) {
        $timeout(function () {
          $scope.downloadProgress = (progress.loaded / progress.total) * 100;
        })
      });
  }
   
  $scope.fileUpload = function (targetFile) {
     // Destination URL 
     var url = "http://example.gajotres.net/upload/upload.php";
      
     //File for Upload
     var targetPath = cordova.file.externalRootDirectory + "logo_radni.png";
      
     // File name only
     var filename = targetPath.split("/").pop();
      
     var options = {
          fileKey: "file",
          fileName: filename,
          chunkedMode: false,
          mimeType: "image/jpg",
          params : {'directory':'upload', 'fileName':filename}
      };
           
      $cordovaFileTransfer.upload(url, targetPath, options).then(function (result) {
          console.log("SUCCESS: " + JSON.stringify(result.response));
      }, function (err) {
          console.log("ERROR: " + JSON.stringify(err));
      }, function (progress) {
          // PROGRESS HANDLING GOES HERE
      });
  }
})




.controller("FileController", function($scope, $ionicPlatform, $ionicLoading) {
 
    $scope.download = function() {
        $ionicLoading.show({
      template: 'Loading...'
    });

         $ionicPlatform.ready(function() {

          // Request Quota (only for File System API)  
// window.webkitStorageInfo.requestQuota(PERSISTENT, 1024*1024, function(grantedBytes) {
//   window.webkitRequestFileSystem(PERSISTENT, grantedBytes, onInitFs, errorHandler); 
// }, function(e) {
//   console.log('Error', e); 
// });

    window.requestFileSystem(1, 0, function(fs) {
        fs.root.getDirectory(
            "ExampleProject",
            {
                create: true
            },
            function(dirEntry) {
                dirEntry.getFile(
                    "test.png", 
                    {
                        create: true, 
                        exclusive: false
                    }, 
                    function gotFileEntry(fe) {
                        var p = fe.toURL();
                        fe.remove();
                        ft = new FileTransfer();
                        ft.download(
                            encodeURI("http://ionicframework.com/img/ionic-logo-blog.png"),
                            p,
                            function(entry) {
                                $ionicLoading.hide();
                                $scope.imgFile = entry.toURL();
                            },
                            function(error) {
                                $ionicLoading.hide();
                                alert("Download Error Source -> " + error.source);
                            },
                            false,
                            null
                        );
                    }, 
                    function() {
                        $ionicLoading.hide();
                        console.log("Get file failed");
                    }
                );
            }
        );
    },
    function() {
        $ionicLoading.hide();
        console.log("Request for filesystem failed");
    });

  });


    }
 
    $scope.load = function() {
    
    }
 
})

     
.controller('PlaylistCtrl', function($scope, $stateParams) {
});
