angular.module('starter.services', [])
 .constant('apiUrl', 'http://127.0.0.1:8008/theBuddhistApp/')

.service('dharmaService', [ '$http', '$q', 'apiUrl', function($http, $q, apiUrl) {
    return {
       listCategories: function(menuItem) {

           var deferred = $q.defer(); 
         // return  $http.get('./js/gatha_category.json');
         // $http.get(apiUrl + 'gatha/categories')
         $http.get('./js/gatha_category.json',
           {
                 params : {
                  type : menuItem
                 }
            })
           .then(function(response) {
               if (response.status == 200) {
               	deferred.resolve(response.data);
               } else {
               	deferred.reject('Error retrieving list [ Category type: ]');
               }
             });
            return deferred.promise;

        },
 

       getListByCategory: function(categoryType) {
           var deferred = $q.defer();

            // $http.get(apiUrl + 'gatha/categories')
           $http.get('./js/gatha_category.json',
             {
                 params : {
                 	categType : categoryType
                 }
            })
           .then(function(response) {
               if (response.status == 200) {
               	deferred.resolve(response.data);
               } else {
               	deferred.reject('Error retrieving list [ Category type: ' + categoryType + ']');
               }
           });
           return deferred.promise;
       },

      getInfoById: function(itemId) {
           var deferred = $q.defer();

            // $http.get(apiUrl + 'gatha/categories')
           $http.get('./js/gatha_category.json',
             {
                 params : {
                  id : itemId
                 }
            })
           .then(function(response) {
               if (response.status == 200) {
                deferred.resolve(response.data);
               } else {
                deferred.reject('Error retrieving item details [ Item id: ' + itemId + ']');
               }
           });
           return deferred.promise;
       },

      search: function(queryTxt) {
             var deferred = $q.defer();

              // $http.get(apiUrl + 'search/)
             $http.get('./js/gatha_category.json',
               {
                   params : {
                    query : queryTxt
                   }
              })
             .then(function(response) {
                 if (response.status == 200) {
                  deferred.resolve(response.data);
                 } else {
                  deferred.reject('Error retrieving items [ Search query: ' + query + ']');
                 }
             });
             return deferred.promise;
         } 
     }

}]);