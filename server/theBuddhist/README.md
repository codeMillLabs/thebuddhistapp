# The Buddhist App | Management Console #

-- Description

### Installation dependencies ###

The following dependencies are necessary: 

 - Java 8
 - bower
 - maven 3 

### Building and starting the server ###

To build the backend and start the server, run the following command on the root folder of the repository:

    mvn clean install tomcat7:run-war -Dspring.profiles.active=test

The spring test profile will activate an in-memory database. After the server starts, the application is accessible at the following URL:

    http://localhost:8080/



### Backend Overview ###

The backend is based on Java 8, Spring 4, JPA 2/ Hibernate 4. The Spring configuration is based on Java. The main Spring modules used where Spring MVC and Spring Security. The backend was built using the DDD approach, which includes a domain model, services, repositories and DTOs for frontend/backend data transfer. 

The REST web services are based on Spring MVC and JSON. The unit tests are made with spring test and the REST API functional tests where made using [Spring test MVC](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/testing.html#spring-mvc-test-framework).


#### REST API ####

The REST API of the backend is composed of 3 services:

##### Authentication Service #####

Url           |Verb          | Description
--------------|------------- | -------------
/authenticate |POST          | authenticates the user
/logout |POST          | ends the current session






### Testing code coverage ###


## Installation instructions
   
    mvn clean install


### How to run the project against a non-in-memory database ###

This command starts the application with a local postgresql database:

    mvn clean install tomcat7:run-war -Dspring.profiles.active=development

