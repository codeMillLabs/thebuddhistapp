# Buddhist App Consoler | REST-Service end points #

## End points ##

http://localhost:8080/buddhist-app-console/home
 
  
## Contents ##  

### ADD ###
  Method : POST
  URL : http://localhost:8080/buddhist-app-console/content/add
      {
        "title": "Dharma Talk-1",
        "description": "Dharma Talk-1",
        "category": "Dharma",
        "author": "Rev. A. Thero",
        "publishedBy": "codeMill Apps",
        "publishedDate": 1451750423000,
        "thumbUrl": "default.img",
        "sourceUrl": "source.mp3"
        }
        
### Modify ###
  Method : POST
  URL : http://localhost:8080/buddhist-app-console/content/modify
      {
        "id" : 677802970557032
        "title": "Dharma Talk-1",
        "description": "Dharma Talk-1",
        "category": "Dharma",
        "author": "Rev. A. Thero",
        "publishedBy": "codeMill Apps",
        "publishedDate": 1451750423000,
        "thumbUrl": "default.img",
        "sourceUrl": "source.mp3"
        }
        
### Remove ###
  Method : GET
  URL : http://localhost:8080/buddhist-app-console/content/remove/{id}
  
### Latest Contents ###
  Method : GET
  URL : http://localhost:8080/buddhist-app-console/content/latest 
  
### Sorted list Contents ###
  Method : GET
  URL : http://localhost:8080/buddhist-app-console/content/list  
  
### Search Contents ###
  Method : GET
  URL : http://localhost:8080/buddhist-app-console/content/search/{text}
      
  
    
          
  
## Events ##

### ADD ###
  Method : POST
  URL : http://localhost:8080/buddhist-app-console/event/add
      {
        "eventName": "Blood Donation @ Colombo",
        "description": "Blood Donation @ Colombo",
        "location": "Dehiwela",
        "country": "Sri Lanka",
        "organizedBy": "codeMill Pvt Ltd.",
        "eventScheduleTime": 1451750423000,
        "priorityLevel": 10
      }
    
### Modify ###
  Method : POST
  URL : http://localhost:8080/buddhist-app-console/event/modify
      {
        "id" : 677802970557032
        "eventName": "Blood Donation @ Colombo",
        "description": "Blood Donation @ Colombo",
        "location": "Dehiwela",
        "country": "Sri Lanka",
        "organizedBy": "codeMill Pvt Ltd.",
        "eventScheduleTime": 1451750423000,
        "priorityLevel": 10
      }
      
### Remove ###      
  Method : GET
  URL : http://localhost:8080/buddhist-app-console/event/remove/{id}
   
    
           
           
           
                                