/*
 * Copyright 2015 Amila Silva (amilasilva88@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codemill.buddhist.app.console.config.root;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.node.NodeBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/**
 *
 * Elastic search client connection configurations
 *
 * @author Amila Silva
 * @since 8.0
 *
 */
@Configuration
@EnableElasticsearchRepositories(basePackages = { "com.codemill.buddhist.app.console.repositories" })
public class ElasticSearchConfig {

	// @Value("search.engine.cluster.name")
	private String clusterName = "the_buddhist";

	// @Value("search.engine.host")
	private String host = "127.0.0.1";

	// @Value("search.engine.port")
	private String port = "9300";

	// @Value("search.engine.local")
	private String isLocal = "true";

	// @Value("search.engine.local.home")
	private String searchDataLocalHome = "./data/the_buddhist";

	@Bean
	public ElasticsearchOperations elasticsearchTemplate() {

		if (Boolean.valueOf(isLocal)) {
			ImmutableSettings.Builder settings = ImmutableSettings
					.settingsBuilder();
			settings.put("path.home", searchDataLocalHome);
			return new ElasticsearchTemplate(NodeBuilder.nodeBuilder()
					.local(true).settings(settings).node().client());
		} else {
			ImmutableSettings.Builder settings = ImmutableSettings
					.settingsBuilder();
			settings.put("cluster.name", clusterName);

			TransportClient client = new TransportClient(settings);
			TransportAddress address = new InetSocketTransportAddress(host,
					9300);
			client.addTransportAddress(address);
			return new ElasticsearchTemplate(client);
		}

		// Settings settings =
		// ImmutableSettings.settingsBuilder().put("cluster.name",
		// "cc-data-cluster").build();
		//
		// Client client = new TransportClient(settings)
		// .addTransportAddress(new InetSocketTransportAddress("127.0.0.1",
		// 9300));

		// TransportClient client = new TransportClient(settings);
		// TransportAddress address = new
		// InetSocketTransportAddress("127.0.0.1", 9300);
		// client.addTransportAddress(address);
		//
		// return new
		// ElasticsearchTemplate(NodeBuilder.nodeBuilder().client(true).node().client());
		// return new ElasticsearchTemplate(client);
	}
}
