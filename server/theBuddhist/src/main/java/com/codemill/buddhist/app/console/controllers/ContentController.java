/*
 * Copyright 2015 Amila Silva (amilasilva88@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codemill.buddhist.app.console.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.codemill.buddhist.app.console.dto.ContentListDTO;
import com.codemill.buddhist.app.console.model.Content;
import com.codemill.buddhist.app.console.services.ContentService;

/**
 *
 * REST service for handle content
 *
 */

@RestController
@RequestMapping("/content")
public class ContentController {

	private static final Logger log = LoggerFactory
			.getLogger(ContentController.class);

	@Autowired
	private ContentService contentService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST, value = "/add")
	public Content add(@RequestBody Content content) {

		content.setId(System.nanoTime());
		contentService.save(content);
		return content;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST, value = "/modify")
	public Content modify(@RequestBody Content content) {
		contentService.save(content);
		return content;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.DELETE, value = "/remove/{id}")
	public void remove(@PathVariable Long id) {
		contentService.remove(id);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, value = "/list/{category}")
	public ContentListDTO findByCategory(
			@PathVariable("category") String category,
			@RequestParam(value = "pageNo", defaultValue = "0", required = false) int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize) {

		Page<Content> contentPage = contentService.findByCategory(category,
				new PageRequest(pageNo, pageSize));
		ContentListDTO contentDTO = new ContentListDTO();
		contentDTO.setPageNo(pageNo);
		contentDTO.setPageSize(pageSize);
		contentDTO.setTotalNoOfElements(contentPage.getTotalElements());
		contentDTO.setNoOfPages(contentPage.getTotalPages());
		contentDTO.setContents(contentPage.getContent());

		log.info("Find all content, No of elements found :{}",
				contentPage.getNumberOfElements());

		return contentDTO;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, value = "/latest")
	public ContentListDTO findAllLatestContents(
			@RequestParam(value = "pageNo", defaultValue = "0", required = false) int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize) {

		Page<Content> contentPage = contentService.findAllWithSortedBy(
				"publishedDate", "DESC", new PageRequest(pageNo, pageSize));
		ContentListDTO contentDTO = new ContentListDTO();
		contentDTO.setPageNo(pageNo);
		contentDTO.setPageSize(pageSize);
		contentDTO.setTotalNoOfElements(contentPage.getTotalElements());
		contentDTO.setNoOfPages(contentPage.getTotalPages());
		contentDTO.setContents(contentPage.getContent());

		log.info("Find all content, No of elements found :{}",
				contentPage.getNumberOfElements());

		return contentDTO;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public ContentListDTO findAllContects(
			@RequestParam(value = "pageNo", defaultValue = "0", required = false) int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize) {

		Page<Content> contentPage = contentService.findAllWithSortedBy("title",
				"ASC", new PageRequest(pageNo, pageSize));
		ContentListDTO contentDTO = new ContentListDTO();
		contentDTO.setPageNo(pageNo);
		contentDTO.setPageSize(pageSize);
		contentDTO.setTotalNoOfElements(contentPage.getTotalElements());
		contentDTO.setNoOfPages(contentPage.getTotalPages());
		contentDTO.setContents(contentPage.getContent());

		log.info("Find all content, No of elements found :{}",
				contentPage.getNumberOfElements());

		return contentDTO;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, value = "/search/{text}")
	public ContentListDTO searchByText(
			@PathVariable("text") String text,
			@RequestParam(value = "pageNo", defaultValue = "0", required = false) int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize) {

		Page<Content> contentPage = contentService.findByString(text,
				new PageRequest(pageNo, pageSize));
		ContentListDTO contentDTO = new ContentListDTO();
		contentDTO.setPageNo(pageNo);
		contentDTO.setPageSize(pageSize);
		contentDTO.setTotalNoOfElements(contentPage.getTotalElements());
		contentDTO.setNoOfPages(contentPage.getTotalPages());
		contentDTO.setContents(contentPage.getContent());

		log.info("Search By Text, No of elements found :{}",
				contentPage.getNumberOfElements());

		return contentDTO;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, value = "/searchByTitle/{title}")
	public ContentListDTO searchByTitle(
			@PathVariable("title") String title,
			@RequestParam(value = "pageNo", defaultValue = "0", required = false) int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize) {

		Page<Content> contentPage = contentService.findByTitleOrDescription(
				title, title, new PageRequest(pageNo, pageSize));
		ContentListDTO contentDTO = new ContentListDTO();
		contentDTO.setPageNo(pageNo);
		contentDTO.setPageSize(pageSize);
		contentDTO.setTotalNoOfElements(contentPage.getTotalElements());
		contentDTO.setNoOfPages(contentPage.getTotalPages());
		contentDTO.setContents(contentPage.getContent());

		log.info("Search by Title, No of elements found :{}",
				contentPage.getNumberOfElements());

		return contentDTO;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, value = "/findByFacet/{facet}")
	public ContentListDTO findByFacet(
			@PathVariable("facet") String facet,
			@RequestParam(value = "pageNo", defaultValue = "0", required = false) int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize) {

		Page<Content> contentPage = contentService.getFacetByField("category",
				"category");
		ContentListDTO contentDTO = new ContentListDTO();
		contentDTO.setPageNo(pageNo);
		contentDTO.setPageSize(pageSize);
		contentDTO.setTotalNoOfElements(contentPage.getTotalElements());
		contentDTO.setNoOfPages(contentPage.getTotalPages());
		contentDTO.setContents(contentPage.getContent());

		log.info("Find By Facet, No of elements found :{}",
				contentPage.getNumberOfElements());

		return contentDTO;
	}

	@ExceptionHandler(Throwable.class)
	public ResponseEntity<String> errorHandler(Throwable e) {
		log.error(e.getMessage(), e);

		return new ResponseEntity<String>("Error !" + e.getMessage(),
				HttpStatus.BAD_REQUEST);
	}
}
