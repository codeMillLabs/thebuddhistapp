/*
 * Copyright 2015 Amila Silva (amilasilva88@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codemill.buddhist.app.console.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.codemill.buddhist.app.console.dto.EventListDTO;
import com.codemill.buddhist.app.console.model.Event;
import com.codemill.buddhist.app.console.services.EventService;

/**
 *
 * REST service for handle content
 *
 */

@RestController
@RequestMapping("/event")
public class EventController {

	private static final Logger log = LoggerFactory
			.getLogger(EventController.class);

	@Autowired
	private EventService eventService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST, value = "/add")
	public Event add(@RequestBody Event event) {

		event.setId(System.nanoTime());
		eventService.save(event);
		return event;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST, value = "/modify")
	public Event modify(@RequestBody Event event) {
		eventService.save(event);
		return event;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.DELETE, value = "/remove/{id}")
	public void remove(@PathVariable Long id) {
		eventService.remove(id);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public EventListDTO findAllEvents(
			@RequestParam(value = "pageNo", defaultValue = "0", required = false) int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize) {

		Page<Event> eventsPage = eventService.findAll(new PageRequest(pageNo,
				pageSize));
		EventListDTO eventsDTO = new EventListDTO();
		eventsDTO.setPageNo(pageNo);
		eventsDTO.setPageSize(pageSize);
		eventsDTO.setTotalNoOfElements(eventsPage.getTotalElements());
		eventsDTO.setNoOfPages(eventsPage.getTotalPages());
		eventsDTO.setEvents(eventsPage.getContent());

		log.info("Find all events, No of elements found :{}",
				eventsPage.getNumberOfElements());

		return eventsDTO;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, value = "/list/{text}")
	public EventListDTO findByText(
			@PathVariable("text") String text,
			@RequestParam(value = "pageNo", defaultValue = "0", required = false) int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize) {

		Page<Event> eventsPage = eventService.findByString(text,
				new PageRequest(pageNo, pageSize));
		EventListDTO eventsDTO = new EventListDTO();
		eventsDTO.setPageNo(pageNo);
		eventsDTO.setPageSize(pageSize);
		eventsDTO.setTotalNoOfElements(eventsPage.getTotalElements());
		eventsDTO.setNoOfPages(eventsPage.getTotalPages());
		eventsDTO.setEvents(eventsPage.getContent());

		log.info("Find by text, No of elements found :{}",
				eventsPage.getNumberOfElements());

		return eventsDTO;
	}

	@ExceptionHandler(Throwable.class)
	public ResponseEntity<String> errorHandler(Throwable e) {
		log.error(e.getMessage(), e);

		return new ResponseEntity<String>("Error !" + e.getMessage(),
				HttpStatus.BAD_REQUEST);
	}
}
