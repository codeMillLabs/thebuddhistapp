package com.codemill.buddhist.app.console.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.codemill.buddhist.app.console.model.Content;

public class ContentListDTO implements Serializable {

	private static final long serialVersionUID = -6223384129430856230L;

	private int pageNo = 0;
	private int pageSize = 10;
	private Long totalNoOfElements = 0L;
	private int noOfPages = 0;
	private List<Content> contents;

	/**
	 * @return the pageNo
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * @param pageNo
	 *            the pageNo to set
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize
	 *            the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the contents
	 */
	public List<Content> getContents() {
		if (contents == null) {
			contents = new ArrayList<Content>();
		}
		return contents;
	}

	/**
	 * @param contents
	 *            the contents to set
	 */
	public void setContents(List<Content> contents) {
		this.contents = contents;
	}

	/**
	 * @return the totalNoOfElements
	 */
	public Long getTotalNoOfElements() {
		return totalNoOfElements;
	}

	/**
	 * @param totalNoOfElements
	 *            the totalNoOfElements to set
	 */
	public void setTotalNoOfElements(Long totalNoOfElements) {
		this.totalNoOfElements = totalNoOfElements;
	}

	/**
	 * @return the noOfPages
	 */
	public int getNoOfPages() {
		return noOfPages;
	}

	/**
	 * @param noOfPages
	 *            the noOfPages to set
	 */
	public void setNoOfPages(int noOfPages) {
		this.noOfPages = noOfPages;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String
				.format("ContentListDTO [pageNo=%s, pageSize=%s, totalNoOfElements=%s, noOfPages=%s, contents=%s]",
						pageNo, pageSize, totalNoOfElements, noOfPages,
						contents);
	}

}
