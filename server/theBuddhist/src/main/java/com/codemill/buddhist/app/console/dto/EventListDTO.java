package com.codemill.buddhist.app.console.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.codemill.buddhist.app.console.model.Event;

public class EventListDTO implements Serializable {

	private static final long serialVersionUID = -4837076920693659945L;

	private int pageNo = 0;
	private int pageSize = 10;
	private Long totalNoOfElements = 0L;
	private int noOfPages = 0;
	private List<Event> events;

	/**
	 * @return the pageNo
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * @param pageNo
	 *            the pageNo to set
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize
	 *            the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the totalNoOfElements
	 */
	public Long getTotalNoOfElements() {
		return totalNoOfElements;
	}

	/**
	 * @param totalNoOfElements
	 *            the totalNoOfElements to set
	 */
	public void setTotalNoOfElements(Long totalNoOfElements) {
		this.totalNoOfElements = totalNoOfElements;
	}

	/**
	 * @return the events
	 */
	public List<Event> getEvents() {
		if (events == null) {
			events = new ArrayList<Event>();
		}
		return events;
	}

	/**
	 * @param events
	 *            the events to set
	 */
	public void setEvents(List<Event> events) {
		this.events = events;
	}

	/**
	 * @return the noOfPages
	 */
	public int getNoOfPages() {
		return noOfPages;
	}

	/**
	 * @param noOfPages
	 *            the noOfPages to set
	 */
	public void setNoOfPages(int noOfPages) {
		this.noOfPages = noOfPages;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String
				.format("EventListDTO [pageNo=%s, pageSize=%s, totalNoOfElements=%s, noOfPages=%s, events=%s]",
						pageNo, pageSize, totalNoOfElements, noOfPages, events);
	}

}
