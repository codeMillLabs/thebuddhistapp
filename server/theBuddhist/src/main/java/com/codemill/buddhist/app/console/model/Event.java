/*
 * Copyright 2015 Amila Silva (amilasilva88@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codemill.buddhist.app.console.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @author Amila Silva
 */
@Document(indexName = "events", type = "event", shards = 1, replicas = 0, refreshInterval = "-1", indexStoreType = "memory")
public class Event implements Serializable {

	private static final long serialVersionUID = 3172987478066037503L;
	
	@Id
	private Long id;
	private String eventName;
	private String description;
	private String location;
	private String country;
	private String organizedBy;
	private Long eventScheduleTime = System.currentTimeMillis();
	private int priorityLevel = -1;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @param eventName
	 *            the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the organizedBy
	 */
	public String getOrganizedBy() {
		return organizedBy;
	}

	/**
	 * @param organizedBy
	 *            the organizedBy to set
	 */
	public void setOrganizedBy(String organizedBy) {
		this.organizedBy = organizedBy;
	}

	/**
	 * @return the eventScheduleTime
	 */
	public Long getEventScheduleTime() {
		return eventScheduleTime;
	}

	/**
	 * @param eventScheduleTime
	 *            the eventScheduleTime to set
	 */
	public void setEventScheduleTime(Long eventScheduleTime) {
		this.eventScheduleTime = eventScheduleTime;
	}

	/**
	 * @return the priorityLevel
	 */
	public int getPriorityLevel() {
		return priorityLevel;
	}

	/**
	 * @param priorityLevel
	 *            the priorityLevel to set
	 */
	public void setPriorityLevel(int priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String
				.format("Event [id=%s, eventName=%s, description=%s, location=%s, country=%s, organizedBy=%s, eventScheduleTime=%s, priorityLevel=%s]",
						id, eventName, description, location, country,
						organizedBy, eventScheduleTime, priorityLevel);
	}

}
