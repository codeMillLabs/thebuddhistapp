/*
 * Copyright 2015 Amila Silva (amilasilva88@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codemill.buddhist.app.console.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.codemill.buddhist.app.console.model.Content;

/**
 * @author Amila Silva
 *
 */
public interface ContentRepository extends
		ElasticsearchRepository<Content, Long> {

	Page<Content> findByCategory(String category, Pageable pageable);

	Page<Content> findByTitleOrDescription(String title, String description,
			Pageable pageable);

	Page<Content> findByAuthorStartingWith(String author, Pageable pageable);

}
