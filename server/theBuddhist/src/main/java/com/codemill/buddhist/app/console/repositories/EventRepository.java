package com.codemill.buddhist.app.console.repositories;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.codemill.buddhist.app.console.model.Event;

public interface EventRepository extends ElasticsearchRepository<Event, Long> {

}
