/*
 * Copyright 2015 Amila Silva (amilasilva88@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codemill.buddhist.app.console.services;

import javax.annotation.Resource;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.elasticsearch.core.facet.request.TermFacetRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import com.codemill.buddhist.app.console.model.Content;
import com.codemill.buddhist.app.console.repositories.ContentRepository;

/**
 * @author Amila Silva
 *
 */
@Service
public class ContentService {

	private static final Logger log = LoggerFactory
			.getLogger(ContentService.class);

	@Resource
	private ContentRepository contentRepository;

	public void save(Content content) {
		contentRepository.save(content);

		log.info("Content detail saved, {}", content);
	}

	public Page<Content> findAll(Pageable pageable) {
		Page<Content> contentPage = contentRepository.findAll(pageable);

		log.info("Find all content details, {}, No of elements :{}", contentPage,
				contentPage.getNumberOfElements());

		return contentPage;
	}

	public void remove(Long id) {
		contentRepository.delete(id);
	}

	public Page<Content> findAllWithSortedBy(String sortField,
			String direction, Pageable pageable) {

		BoolQueryBuilder query = QueryBuilders.boolQuery();
		query.must(QueryBuilders.matchAllQuery());

		SearchQuery searchQuery = new NativeSearchQueryBuilder()
				.withQuery(query).withPageable(pageable).build();

		Direction sortDirection = direction.equalsIgnoreCase("ASC") ? Direction.ASC
				: Direction.DESC;
		searchQuery.addSort(new Sort(sortDirection, sortField));

		log.debug("Find query :{}", query);

		Page<Content> contentListPage = contentRepository.search(searchQuery);

		log.info("Find all content details, {}, No of elements:{}",
				contentListPage, contentListPage.getNumberOfElements());

		return contentListPage;
	}

	public Page<Content> findByCategory(String category, Pageable pageable) {
		Page<Content> contentListPage = contentRepository.findByCategory(
				category, pageable);
		log.info("Find all content by category: {}, No of elements:{}",
				category, contentListPage.getNumberOfElements());

		return contentListPage;
	}

	public Page<Content> findByTitleOrDescription(String title,
			String description, Pageable pageable) {
		Page<Content> contentListPage = contentRepository
				.findByTitleOrDescription(title, description, pageable);
		log.info(
				"Find all content by title or description: {}, No of elements:{}",
				title, contentListPage.getNumberOfElements());

		return contentListPage;
	}

	public Page<Content> findByAuthorStartingWith(String author,
			Pageable pageable) {
		Page<Content> contentListPage = contentRepository
				.findByAuthorStartingWith(author, pageable);
		log.info("Find all content by author: {}, No of elements:{}", author,
				contentListPage.getNumberOfElements());

		return contentListPage;
	}

	public Page<Content> getFacetByField(String facetField, String field) {
		BoolQueryBuilder query = QueryBuilders.boolQuery();
		query.must(QueryBuilders.matchAllQuery());

		TermFacetRequest facet = new TermFacetRequest("*");
		facet.setFields(facetField);
		SearchQuery searchQuery = new NativeSearchQueryBuilder()
				.withQuery(query).withFacet(facet).build();

		searchQuery.addSort(new Sort(Direction.DESC, "publishedDate"));
		searchQuery.addSort(new Sort(Direction.ASC, "title"));
		log.debug("Find query :{}", query);

		Page<Content> contentListPage = contentRepository.search(searchQuery);
		log.info("Find all event details, {}, No of elements :{}",
				contentListPage, contentListPage.getNumberOfElements());

		return contentListPage;
	}

	public Page<Content> findByString(String text, Pageable pageable) {

		BoolQueryBuilder innerQuery = QueryBuilders.boolQuery();
		innerQuery.should(QueryBuilders.matchQuery("title", text))
				.should(QueryBuilders.matchQuery("description", text))
				.should(QueryBuilders.matchQuery("author", text))
				.should(QueryBuilders.matchQuery("publishedBy", text));

		BoolQueryBuilder query = QueryBuilders.boolQuery();
		query.must(innerQuery);

		SearchQuery searchQuery = new NativeSearchQueryBuilder()
				.withQuery(query).withPageable(pageable).build();

		searchQuery.addSort(new Sort(Direction.DESC, "publishedDate"));
		searchQuery.addSort(new Sort(Direction.ASC, "title"));
		log.debug("Find query :{}", query);

		Page<Content> contentListPage = contentRepository.search(searchQuery);

		log.info("Find all event details, {}, No of elements :{}",
				contentListPage, contentListPage.getNumberOfElements());

		return contentListPage;
	}

}
