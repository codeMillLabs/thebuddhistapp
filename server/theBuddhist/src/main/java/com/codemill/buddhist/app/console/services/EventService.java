/*
 * Copyright 2015 Amila Silva (amilasilva88@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codemill.buddhist.app.console.services;

import javax.annotation.Resource;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import com.codemill.buddhist.app.console.model.Event;
import com.codemill.buddhist.app.console.repositories.EventRepository;

/**
 * 
 * @author Amila Silva
 *
 */
@Service
public class EventService {

	private static final Logger log = LoggerFactory
			.getLogger(EventService.class);

	@Resource
	private EventRepository eventReposiotry;

	public void save(Event event) {
		eventReposiotry.save(event);

		log.info("Event detail saved, {}", event);
	}

	public Page<Event> findAll(Pageable pageable) {
		Page<Event> eventPage = eventReposiotry.findAll(pageable);

		log.info("Find all event details, {}, No of elements :{}", eventPage,
				eventPage.getNumberOfElements());

		return eventPage;
	}

	public void remove(Long id) {
		eventReposiotry.delete(id);
	}

	public Page<Event> findAllWithSortedBy(String sortField, String direction,
			Pageable pageable) {

		BoolQueryBuilder query = QueryBuilders.boolQuery();
		query.must(QueryBuilders.matchAllQuery());

		SearchQuery searchQuery = new NativeSearchQueryBuilder()
				.withQuery(query).withPageable(pageable).build();

		Direction sortDirection = direction.equalsIgnoreCase("ASC") ? Direction.ASC
				: Direction.DESC;
		searchQuery.addSort(new Sort(sortDirection, sortField));

		log.debug("Find query :{}", query);

		Page<Event> eventListPage = eventReposiotry.search(searchQuery);

		log.info("Find all event details, {}, No of elements :{}",
				eventListPage, eventListPage.getNumberOfElements());

		return eventListPage;
	}

	public Page<Event> findByString(String text, Pageable pageable) {

		BoolQueryBuilder innerQuery = QueryBuilders.boolQuery();
		innerQuery.should(QueryBuilders.matchQuery("eventName", text))
				.should(QueryBuilders.matchQuery("description", text))
				.should(QueryBuilders.matchQuery("location", text))
				.should(QueryBuilders.matchQuery("country", text))
				.should(QueryBuilders.matchQuery("organizedBy", text));

		BoolQueryBuilder query = QueryBuilders.boolQuery();
		query.must(innerQuery);

		SearchQuery searchQuery = new NativeSearchQueryBuilder()
				.withQuery(query).withPageable(pageable).build();

		searchQuery.addSort(new Sort(Direction.DESC, "priorityLevel"));
		searchQuery.addSort(new Sort(Direction.DESC, "eventScheduleTime"));
		searchQuery.addSort(new Sort(Direction.ASC, "eventName"));
		log.debug("Find query :{}", query);

		Page<Event> eventListPage = eventReposiotry.search(searchQuery);

		log.info("Find all event details, {}, No of elements :{}",
				eventListPage, eventListPage.getNumberOfElements());

		return eventListPage;
	}
}
