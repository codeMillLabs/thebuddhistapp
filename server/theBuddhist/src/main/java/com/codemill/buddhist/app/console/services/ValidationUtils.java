/*
 * Copyright 2015 Amila Silva (amilasilva88@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codemill.buddhist.app.console.services;

import java.util.regex.Pattern;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * Validation utility methods
 *
 */
public final class ValidationUtils {

	private ValidationUtils() {
		throw new NotImplementedException(
				"Utility classes cannot be instantiated");
	}

	public static void assertNotBlank(String value, String message) {
		if (StringUtils.isBlank(value)) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void assertNotNull(Object obj, String message) {
		if (obj == null) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void assertMinimumLength(String value, int length,
			String message) {
		if (value.length() < length) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void assertMatches(String value, Pattern regex, String message) {
		if (!regex.matcher(value).matches()) {
			throw new IllegalArgumentException(message);
		}
	}
}
