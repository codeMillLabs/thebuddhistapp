///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Device Module Controllers
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
'use strict'

angular.module('assetScheduleModule.controllers', ['datePicker', 'campaignModule.services', 'assetModule.services', 'assetScheduleModule.services'])

angular.module('assetScheduleModule.controllers').controller('AssetScheduleListController', 
     ['$scope', 'campaignService', 'assetService',  'assetScheduleService',
      function($scope, campaignService, assetService, assetScheduleService) {

     $scope.campaigns = [];
     $scope.assets = [];
     $scope.search = { campaignId: '', assetId: ''};

      $scope.listAllCampaigns = function() {
        var promise = campaignService.listAll();
        promise.then(function (data) {
            $scope.campaigns = data;
        },
        function (errorMessage) {
           
        });
      }    

      $scope.listAllAssets = function() {
        var promise = assetService.listAll();
        promise.then(function (data) {
             $scope.assets = data;
         },
        function (errorMessage) {
                
         });
      } 


      $scope.searchData = function() {
        var promise = {};
        if($scope.search.campaignId) {
            promise = assetScheduleService.listByCampaignId($scope.search.campaignId);
        } else if ($scope.search.assetId) {
            promise = assetScheduleService.listByAssetId($scope.search.assetId);
        }

        if (promise.then) {
            promise.then(function (data) {
               $scope.schedules = data;
            },
            function (errorMessage) {
              // $scope.message = errorMessage + " ERROR";
            });
        }
      } 
}]);

angular.module('assetScheduleModule.controllers').controller('AssetScheduleDetailController', 
     ['$scope', 'assetScheduleService', function($scope, assetScheduleService) {

     var promise = assetScheduleService.getById($scope.assetId);
      promise.then(function (data) {
           $scope.schedule = data;
        },
        function (errorMessage) {
          // $scope.message = errorMessage + " ERROR";
        });
}]);

angular.module('assetScheduleModule.controllers').controller('AssetScheduleModifyController', 
	['$scope', '$timeout', '$location', '$routeParams', 'campaignService', 'assetService', 'assetScheduleService', 
  function($scope, $timeout, $location, $routeParams, campaignService, assetService, assetScheduleService)  {

   $scope.message = 'Asset Schedule Details'
   $scope.schedule = {};
   $scope.addButtonText ="Save Asset Schedule";
   $scope.campaigns = [];
   $scope.assets = [];
   $scope.status_options = [{key: 'PENDING', value: 'Pending'}, {key: 'ACTIVE', value: 'Active'}, {key: 'IN_ACTIVE', value: 'In-Active'}];

   var scheduleId = $routeParams.id;

   $scope.listAllCampaigns = function() {
      var promise = campaignService.listAll();
       promise.then(function (data) {
               $scope.campaigns = data;
            },
            function (errorMessage) {
          
            });
   }   

    $scope.listAllAssets = function() {
      var promise = assetService.listAll();
       promise.then(function (data) {
               $scope.assets = data;
            },
            function (errorMessage) {
             
            });
   } 
   
   
   if(scheduleId) {
      var promise = assetScheduleService.getById(scheduleId);
      promise.then(function (data) {
           $scope.schedule = data;
        },
        function (errorMessage) {
          // $scope.message = errorMessage + " ERROR";
        });
    }

    $scope.saveAssetSchedule = function() {
       $scope.addButtonText = "Saving. . .";
       var promise = {};

        if($scope.schedule.id) {
            promise = assetScheduleService.updateAssetDetails($scope.schedule);
        } else {
            promise = assetScheduleService.addAssetDetails($scope.schedule);
        }
      

        promise.then(function (data) {
             $scope.message = "Asset Schedule Modified";
             $scope.addButtonText = "Saved";

             $timeout(function() {
                $location.path('/list');
             }, 2000); 
               
           },
           function (errorMessage) {
           	 $scope.message = errorMessage + " ERROR";
           });
    }

}]);