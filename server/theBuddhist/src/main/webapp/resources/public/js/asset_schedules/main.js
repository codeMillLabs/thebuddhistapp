'use strict'

angular.module('assetScheduleModule', ['ngRoute', 'assetScheduleModule.controllers']);


// Define routes for devices module
angular.module('assetScheduleModule').config(['$routeProvider', function($routeProvider) {
     
     $routeProvider.when('/list', {
     	templateUrl: 'partials/asset_schedules/asset-schedules-list.html'
     }).
     when('/modify/:id', {
     	templateUrl: 'partials/asset_schedules/asset-schedule-form.html'
     }).
     when('/create', {
     	templateUrl: 'partials/asset_schedules/asset-schedule-form.html'
     }).
     otherwise({
     	redirectTo: '/list'
     });
}]);