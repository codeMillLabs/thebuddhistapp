///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Device Module Services
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
'use strict'

angular.module('assetScheduleModule.services', [])

angular.module('assetScheduleModule.services').service('assetScheduleService', [ '$http', '$q', function($http, $q) {
    return {
       listByCampaignId: function(campaignId) {
           var deferred = $q.defer();

           $http.get('../../assetSchedule/listAll', {
                 params : {
                   id: campaignId
                 }
           })
           .then(function(response) {
               if (response.status == 200) {
               	 deferred.resolve(response.data);
               } else {
               	 deferred.reject('Error retrieving list of asset schedules');
               }

           });

           return deferred.promise;
       },

        listByAssetId: function(assetId) {
           var deferred = $q.defer();

           $http.get('../../assetSchedule/listAllForAsset',{
                 params : {
                   id: assetId
                 }
           })
           .then(function(response) {
               if (response.status == 200) {
                 deferred.resolve(response.data);
               } else {
                 deferred.reject('Error retrieving list of asset schedules');
               }

           });

           return deferred.promise;
       },

       getById: function(assetScheduleId) {
           var deferred = $q.defer();

           $http.get('../../assetSchedule/details',{
                 params : {
                 	 id: assetScheduleId
                 }
           })
           .then(function(response) {
               if (response.status == 200) {
               	 deferred.resolve(response.data);
               } else {
               	 deferred.reject('Error retrieving asset schedule [ Asset schedule Id: ' + assetScheduleId + ']');
               }
           });
           return deferred.promise;
       },

       updateAssetDetails:  function(dirtyAssetScheduleInfo) {
	       	var deferred = $q.defer();

	        $http({ method: 'POST',
                    url: '../../assetSchedule/modify',
                    data:  dirtyAssetScheduleInfo,
                    headers: {
                    	"Content-Type": "application/json",
                    	"Accept": "text/plain, application/json"
                    }
	            })
	           .then(function(response) {
	              if (response.status == 200) {
	               	deferred.resolve(response.data);
	               } else {
	               	deferred.reject('Error updating asset schedule infomation');
	               }

	        });
           return deferred.promise;
       },

       addAssetDetails:  function(dirtyAssetScheduleInfo) {
	       	var deferred = $q.defer();

	        $http({ method: 'POST',
                    url: '../../assetSchedule/add',
                    data:  dirtyAssetScheduleInfo,
                    headers: {
                    	"Content-Type": "application/json",
                    	"Accept": "text/plain, application/json"
                    }
	            })
	           .then(function(response) {
	              if (response.status == 200) {
	                 deferred.resolve(response.data);
	              } else {
	               	 deferred.reject('Error save asset schedules');
	              }
	        });
           return deferred.promise;
       },

       removeAsset:  function(assetScheduleId) {
	       	var deferred = $q.defer();

	         $http.get('../../assetSchedule/remove',{
                 params : {
                 	id: assetScheduleId
                 }
             })
	         .then(function(response) {
	              if (response.status == 200) {
	               	deferred.resolve(response.data);
	              } else {
	               	deferred.reject('Error delete asset schedules');
	              }
	         });
          return deferred.promise;
      }       
    }
}]);