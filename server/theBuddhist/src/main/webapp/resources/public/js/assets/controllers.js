///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Device Module Controllers
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
'use strict'

angular.module('assetModule.controllers', ['assetModule.services'])

angular.module('assetModule.controllers').controller('AssetListController', 
     ['$scope', 'assetService', function($scope, assetService) {

     var promise = assetService.listAll();
      promise.then(function (data) {
           $scope.assets = data;
        },
        function (errorMessage) {
          // $scope.message = errorMessage + " ERROR";
        });
}]);

angular.module('assetModule.controllers').controller('AssetDetailController', 
     ['$scope', 'assetService', function($scope, assetService) {

     var promise = assetService.getById($scope.assetId);
      promise.then(function (data) {
           $scope.asset = data;
        },
        function (errorMessage) {
          // $scope.message = errorMessage + " ERROR";
        });
}]);

angular.module('assetModule.controllers').controller('AssetModifyController', 
	['$scope', '$timeout', '$location', '$routeParams', 'assetService', function($scope, $timeout, $location, $routeParams, assetService)  {

   $scope.message = 'Asset Details'
   $scope.asset = {};
   $scope.addButtonText ="Save Asset";
   $scope.assetTypes = [{key: 'IMAGE', value: 'Image'}, {key:'VIDEO', value:'Video'}, {key:'WEB_URL', value:'Web Link'}];
   $scope.status_options = [{key: 'ACTIVE', value: 'Active'}, {key: 'IN_ACTIVE', value: 'In-Active'}];

   var assetId = $routeParams.id;
   
   if(assetId) {
      var promise = assetService.getById(assetId);
      promise.then(function (data) {
           $scope.asset = data;
        },
        function (errorMessage) {
          // $scope.message = errorMessage + " ERROR";
        });
    }

    $scope.saveAsset = function() {
       $scope.addButtonText = "Saving. . .";
       var promise = {};

        if($scope.asset.id) {
            promise = assetService.updateAssetDetails($scope.asset);
        } else {
            promise = assetService.addAssetDetails($scope.asset);
        }
      

        promise.then(function (data) {
             $scope.message = "Asset Modified";
             $scope.addButtonText = "Saved";

             $timeout(function() {
                $location.path('/list');
             }, 2000); 
               
           },
           function (errorMessage) {
           	 $scope.message = errorMessage + " ERROR";
           });
    }

}]);