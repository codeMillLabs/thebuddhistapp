'use strict'

angular.module('assetModule', ['ngRoute', 'assetModule.controllers']);


// Define routes for devices module
angular.module('assetModule').config(['$routeProvider', function($routeProvider) {
     
     $routeProvider.when('/list', {
     	templateUrl: 'partials/assets/assets-list.html'
     }).
     when('/modify/:id', {
     	templateUrl: 'partials/assets/assets-form.html'
     }).
     when('/create', {
     	templateUrl: 'partials/assets/assets-form.html'
     }).
     otherwise({
     	redirectTo: '/list'
     });
}]);