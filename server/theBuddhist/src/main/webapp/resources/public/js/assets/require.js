///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Defines the javascript files that need to be loaded and their dependencies.
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////

require.config({
    paths: {
        angular: '../../../bower_components/angular/angular',
        angularRoute: '../../../bower_components/angular-route/angular-route',
        angularSmartTable: "../../../bower_components/angular-smart-table/dist/smart-table.js",
        controllers: 'controllers'
    },
    shim: {
        angular: {
            exports: "angular"
        },
        angularSmartTable: {
            deps: ['angular']
        }
    }
});

require(['asset_module'], function () {

    angular.bootstrap(document.getElementById('asset_module'), ['assetModule']);

});