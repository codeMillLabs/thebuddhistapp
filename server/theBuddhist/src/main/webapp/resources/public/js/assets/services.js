///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Device Module Services
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
'use strict'

angular.module('assetModule.services', [])

angular.module('assetModule.services').service('assetService', [ '$http', '$q', function($http, $q) {
    return {
       listAll: function() {
           var deferred = $q.defer();

           $http.get('../../asset/listAll')
           .then(function(response) {
               if (response.status == 200) {
               	 deferred.resolve(response.data);
               } else {
               	 deferred.reject('Error retrieving list of assets');
               }

           });

           return deferred.promise;
       },

       getById: function(assetId) {
           var deferred = $q.defer();

           $http.get('../../asset/details',{
                 params : {
                 	 id: assetId
                 }
           })
           .then(function(response) {
               if (response.status == 200) {
               	 deferred.resolve(response.data);
               } else {
               	 deferred.reject('Error retrieving asset [ Asset Id: ' + assetId + ']');
               }
           });
           return deferred.promise;
       },

       updateAssetDetails:  function(dirtyAssetInfo) {
	       	var deferred = $q.defer();

	        $http({ method: 'POST',
                    url: '../../asset/modify',
                    data:  dirtyAssetInfo,
                    headers: {
                    	"Content-Type": "application/json",
                    	"Accept": "text/plain, application/json"
                    }
	            })
	           .then(function(response) {
	              if (response.status == 200) {
	               	deferred.resolve(response.data);
	               } else {
	               	deferred.reject('Error updating asset infomation');
	               }

	        });
           return deferred.promise;
       },

       addAssetDetails:  function(dirtyAssetInfo) {
	       	var deferred = $q.defer();

	        $http({ method: 'POST',
                    url: '../../asset/add',
                    data:  dirtyAssetInfo,
                    headers: {
                    	"Content-Type": "application/json",
                    	"Accept": "text/plain, application/json"
                    }
	            })
	           .then(function(response) {
	              if (response.status == 200) {
	                 deferred.resolve(response.data);
	              } else {
	               	 deferred.reject('Error save asset');
	              }
	        });
           return deferred.promise;
       },

       removeAsset:  function(assetId) {
	       	var deferred = $q.defer();

	         $http.get('../../asset/remove',{
                 params : {
                 	id: assetId
                 }
             })
	         .then(function(response) {
	              if (response.status == 200) {
	               	deferred.resolve(response.data);
	              } else {
	               	deferred.reject('Error delete asset');
	              }
	         });
          return deferred.promise;
      }       
    }
}]);