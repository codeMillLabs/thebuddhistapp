///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Device Module Controllers
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
'use strict'

angular.module('campaignModule.controllers', ['campaignModule.services'])

angular.module('campaignModule.controllers').controller('CampaignListController', 
     ['$scope', 'campaignService', function($scope, campaignService) {

     var promise = campaignService.listAll();
      promise.then(function (data) {
           $scope.campaigns = data;
        },
        function (errorMessage) {
          // $scope.message = errorMessage + " ERROR";
        });
}]);

angular.module('campaignModule.controllers').controller('CampaignDetailController', 
     ['$scope', 'campaignService', function($scope, campaignService) {

     var promise = campaignService.getById($scope.campaignId);
      promise.then(function (data) {
           $scope.campaign = data;
        },
        function (errorMessage) {
          // $scope.message = errorMessage + " ERROR";
        });
}]);

angular.module('campaignModule.controllers').controller('CampaignModifyController', 
	['$scope', '$timeout', '$location', '$routeParams', 'campaignService', function($scope, $timeout, $location, $routeParams, campaignService)  {

   $scope.message = 'Campaign Details'
   $scope.campaign = {};
   $scope.addButtonText ="Save Campaign";


   var campaignId = $routeParams.id;
   
   if(campaignId) {
      var promise = campaignService.getById(campaignId);
      promise.then(function (data) {
           $scope.campaign = data;
        },
        function (errorMessage) {
          // $scope.message = errorMessage + " ERROR";
        });
    }

    $scope.saveCampaign = function() {
       $scope.addButtonText = "Saving. . .";
       var promise = {};

        if($scope.campaign.id) {
            promise = campaignService.updateCampaignDetails($scope.campaign);
        } else {
            promise = campaignService.addCampaignDetails($scope.campaign);
        }
      

        promise.then(function (data) {
             $scope.message = "Campaign Modified";
             $scope.addButtonText = "Saved";

             $timeout(function() {
                $location.path('/list');
             }, 2000); 
               
           },
           function (errorMessage) {
           	 $scope.message = errorMessage + " ERROR";
           });
    }

}]);