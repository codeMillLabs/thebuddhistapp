'use strict'

angular.module('campaignModule', ['ngRoute', 'campaignModule.controllers']);


// Define routes for devices module
angular.module('campaignModule').config(['$routeProvider', function($routeProvider) {
     
     $routeProvider.when('/list', {
     	templateUrl: 'partials/campaigns/campaigns-list.html'
     }).
     when('/modify/:id', {
     	templateUrl: 'partials/campaigns/campaigns-form.html'
     }).
     when('/create', {
     	templateUrl: 'partials/campaigns/campaigns-form.html'
     }).
     otherwise({
     	redirectTo: '/list'
     });
}]);