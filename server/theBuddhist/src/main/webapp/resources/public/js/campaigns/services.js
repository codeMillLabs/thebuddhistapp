///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Device Module Services
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
'use strict'

angular.module('campaignModule.services', [])

angular.module('campaignModule.services').service('campaignService', [ '$http', '$q', function($http, $q) {
    return {
       listAll: function() {
           var deferred = $q.defer();

           $http.get('../../campaign/listAll')
           .then(function(response) {
               if (response.status == 200) {
               	 deferred.resolve(response.data);
               } else {
               	 deferred.reject('Error retrieving list of campaign');
               }

           });

           return deferred.promise;
       },

       getById: function(campaignId) {
           var deferred = $q.defer();

           $http.get('../../campaign/details',{
                 params : {
                 	 id: campaignId
                 }
           })
           .then(function(response) {
               if (response.status == 200) {
               	 deferred.resolve(response.data);
               } else {
               	 deferred.reject('Error retrieving campaign [ Campaign Id: ' + campaignId + ']');
               }
           });
           return deferred.promise;
       },

       updateCampaignDetails:  function(dirtyCampaignInfo) {
	       	var deferred = $q.defer();

	        $http({ method: 'POST',
                    url: '../../campaign/modify',
                    data:  dirtyCampaignInfo,
                    headers: {
                    	"Content-Type": "application/json",
                    	"Accept": "text/plain, application/json"
                    }
	            })
	           .then(function(response) {
	              if (response.status == 200) {
	               	deferred.resolve(response.data);
	               } else {
	               	deferred.reject('Error updating campaign infomation');
	               }

	        });
           return deferred.promise;
       },

       addCampaignDetails:  function(dirtyCampaignInfo) {
	       	var deferred = $q.defer();

	        $http({ method: 'POST',
                    url: '../../campaign/add',
                    data:  dirtyCampaignInfo,
                    headers: {
                    	"Content-Type": "application/json",
                    	"Accept": "text/plain, application/json"
                    }
	            })
	           .then(function(response) {
	              if (response.status == 200) {
	                 deferred.resolve(response.data);
	              } else {
	               	 deferred.reject('Error registering campaign');
	              }
	        });
           return deferred.promise;
       },

       removeCampaign:  function(campaignId) {
	       	var deferred = $q.defer();

	         $http.get('../../campaign/remove',{
                 params : {
                 	id: campaignId
                 }
             })
	         .then(function(response) {
	              if (response.status == 200) {
	               	deferred.resolve(response.data);
	              } else {
	               	deferred.reject('Error un-registering campaign');
	              }
	         });
          return deferred.promise;
      }       
    }
}]);