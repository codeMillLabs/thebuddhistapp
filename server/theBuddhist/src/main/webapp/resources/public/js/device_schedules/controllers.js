///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Device Module Controllers
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
'use strict'

angular.module('deviceScheduleModule.controllers', ['campaignModule.services', 'assetScheduleModule.services', 'deviceModule.services', 'deviceScheduleModule.services'])

angular.module('deviceScheduleModule.controllers').controller('DeviceScheduleListController', 
     ['$scope', 'deviceService', 'campaignService', 'assetScheduleService', 'deviceScheduleService', 
      function($scope, deviceService, campaignService,  assetScheduleService, deviceScheduleService) {

     $scope.devices = [];
     $scope.campaigns = [];
     $scope.assetSchedules = [];
    
     $scope.search = { deviceId: '', campaignId: '', assetScheduleId: ''};

      $scope.listAllDevices = function() {
        var promise = deviceService.listAll();
        promise.then(function (data) {
            $scope.devices = data;
        },
        function (errorMessage) {
           
        });
      }    

      $scope.listAllCampaigns = function() {
        var promise = campaignService.listAll();
        promise.then(function (data) {
            $scope.campaigns = data;
        },
        function (errorMessage) {
           
        });
      }    

      $scope.listAssetSchedules = function(campaignId) {
        var promise = assetScheduleService.listByCampaignId(campaignId);
        promise.then(function (data) {
            $scope.assetSchedules = data;
        },
        function (errorMessage) {
           
        });
      }    


      $scope.searchData = function() {
        var promise = {};
        if($scope.search.deviceId) {
            promise = deviceScheduleService.listByDeviceId($scope.search.deviceId);
        } else if ($scope.search.assetScheduleId) {
            promise = deviceScheduleService.listByAssetScheduleId($scope.search.assetScheduleId);
        }

        if (promise.then) {
            promise.then(function (data) {
               $scope.schedules = data;
            },
            function (errorMessage) {
              // $scope.message = errorMessage + " ERROR";
            });
        }
      } 
}]);

angular.module('deviceScheduleModule.controllers').controller('DeviceScheduleDetailController', 
     ['$scope', 'deviceScheduleService', function($scope, deviceScheduleService) {

     var promise = deviceScheduleService.getById($scope.scheduleId);
      promise.then(function (data) {
           $scope.schedule = data;
        },
        function (errorMessage) {
          // $scope.message = errorMessage + " ERROR";
        });
}]);

angular.module('deviceScheduleModule.controllers').controller('DeviceScheduleModifyController', 
	['$scope', '$timeout', '$location', '$routeParams', 'deviceService', 'campaignService', 'assetScheduleService', 'deviceScheduleService', 
  function($scope, $timeout, $location, $routeParams, deviceService, campaignService, assetScheduleService, deviceScheduleService)  {

   $scope.message = 'Device Schedule Details'
   $scope.schedule = {};
   $scope.addButtonText ="Save Device Schedule";
   $scope.devices = [];
   $scope.campaigns = [];
   $scope.assetSchedules = [];
   $scope.status_options = [{key: 'PENDING', value: 'Pending'}, {key: 'ACTIVE', value: 'Active'}, 
                            {key: 'IN_ACTIVE', value: 'In-Active'}, {key:'DOWNLOADED', value: 'Downloaded'},
                            {key:'FAILED', value: 'Failed'}, {key: 'DELETED', value: 'Deleted'}];

   var scheduleId = $routeParams.id;

    $scope.listAllDevices = function() {
      var promise = deviceService.listAll();
      promise.then(function (data) {
          $scope.devices = data;
      },
      function (errorMessage) {
         
      });
    }    

    $scope.listAllCampaigns = function() {
      var promise = campaignService.listAll();
      promise.then(function (data) {
          $scope.campaigns = data;
      },
      function (errorMessage) {
         
      });
    }    

    $scope.listAssetSchedules = function(campaignId) {
      var promise = assetScheduleService.listByCampaignId(campaignId);
      promise.then(function (data) {
          $scope.assetSchedules = data;
      },
      function (errorMessage) {
         
      });
    }    
   
   
   if(scheduleId) {
      var promise = deviceScheduleService.getById(scheduleId);
      promise.then(function (data) {
           $scope.schedule = data;
        },
        function (errorMessage) {
          // $scope.message = errorMessage + " ERROR";
        });
    }

    $scope.saveDeviceSchedule = function() {
       $scope.addButtonText = "Saving. . .";
       var promise = {};

        if($scope.schedule.id) {
            promise = deviceScheduleService.updateDeviceSchedule($scope.schedule);
        } else {
            promise = deviceScheduleService.addDeviceSchedule($scope.schedule);
        }
      

        promise.then(function (data) {
             $scope.message = "Device Schedule Modified";
             $scope.addButtonText = "Saved";

             $timeout(function() {
                $location.path('/list');
             }, 2000); 
               
           },
           function (errorMessage) {
           	 $scope.message = errorMessage + " ERROR";
           });
    }

}]);