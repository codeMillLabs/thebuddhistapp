'use strict'

angular.module('deviceScheduleModule', ['ngRoute', 'deviceScheduleModule.controllers']);


// Define routes for devices module
angular.module('deviceScheduleModule').config(['$routeProvider', function($routeProvider) {
     
     $routeProvider.when('/list', {
     	templateUrl: 'partials/device_schedules/device-schedules-list.html'
     }).
     when('/modify/:id', {
     	templateUrl: 'partials/device_schedules/device-schedule-form.html'
     }).
     when('/create', {
     	templateUrl: 'partials/device_schedules/device-schedule-form.html'
     }).
     otherwise({
     	redirectTo: '/list'
     });
}]);