///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Device Module Services
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
'use strict'

angular.module('deviceScheduleModule.services', [])

angular.module('deviceScheduleModule.services').service('deviceScheduleService', [ '$http', '$q', function($http, $q) {
    return {
       listByDeviceId: function(deviceId) {
           var deferred = $q.defer();

           $http.get('../../deviceSchedule/listByDevice', {
                 params : {
                   id: deviceId
                 }
           })
           .then(function(response) {
               if (response.status == 200) {
               	 deferred.resolve(response.data);
               } else {
               	 deferred.reject('Error retrieving list of device schedules');
               }

           });

           return deferred.promise;
       },

      listByAssetScheduleId: function(assetScheduleId) {
           var deferred = $q.defer();

           $http.get('../../deviceSchedule/listByAssetSchedule',{
                 params : {
                   id: assetScheduleId
                 }
           })
           .then(function(response) {
               if (response.status == 200) {
                 deferred.resolve(response.data);
               } else {
                 deferred.reject('Error retrieving list of device schedules');
               }

           });

           return deferred.promise;
       },

       getById: function(deviceScheduleId) {
           var deferred = $q.defer();

           $http.get('../../deviceSchedule/details',{
                 params : {
                 	 id: deviceScheduleId
                 }
           })
           .then(function(response) {
               if (response.status == 200) {
               	 deferred.resolve(response.data);
               } else {
               	 deferred.reject('Error retrieving device schedule [ Device schedule Id: ' + deviceScheduleId + ']');
               }
           });
           return deferred.promise;
       },

       updateDeviceSchedule:  function(dirtyDeviceScheduleInfo) {
	       	var deferred = $q.defer();

	        $http({ method: 'POST',
                    url: '../../deviceSchedule/modify',
                    data:  dirtyDeviceScheduleInfo,
                    headers: {
                    	"Content-Type": "application/json",
                    	"Accept": "text/plain, application/json"
                    }
	            })
	           .then(function(response) {
	              if (response.status == 200) {
	               	deferred.resolve(response.data);
	               } else {
	               	deferred.reject('Error updating device schedule infomation');
	               }

	        });
           return deferred.promise;
       },

       addDeviceSchedule:  function(dirtyDeviceScheduleInfo) {
	       	var deferred = $q.defer();

	        $http({ method: 'POST',
                    url: '../../deviceSchedule/add',
                    data:  dirtyDeviceScheduleInfo,
                    headers: {
                    	"Content-Type": "application/json",
                    	"Accept": "text/plain, application/json"
                    }
	            })
	           .then(function(response) {
	              if (response.status == 200) {
	                 deferred.resolve(response.data);
	              } else {
	               	 deferred.reject('Error save device schedules');
	              }
	        });
           return deferred.promise;
       },

       removeSchedule:  function(deviceScheduleId) {
	       	var deferred = $q.defer();

	         $http.get('../../deviceSchedule/remove',{
                 params : {
                 	id: deviceScheduleId
                 }
             })
	         .then(function(response) {
	              if (response.status == 200) {
	               	deferred.resolve(response.data);
	              } else {
	               	deferred.reject('Error delete device schedules');
	              }
	         });
          return deferred.promise;
      }       
    }
}]);