///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Device Module Controllers
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
'use strict'

angular.module('devicesModule.controllers', ['nvd3', 'deviceModule.services'])

angular.module('devicesModule.controllers').controller('DeviceListController', 
     ['$scope', 'deviceService', function($scope, deviceService) {

     var promise = deviceService.listAll();
      promise.then(function (data) {
           $scope.devices = data;
        },
        function (errorMessage) {
          // $scope.message = errorMessage + " ERROR";
        });
}]);



angular.module('devicesModule.controllers').controller('DeviceDetailViewController', 
    ['$scope', '$timeout', '$location', '$routeParams', 'deviceService', 
     function($scope, $timeout, $location, $routeParams, deviceService){
      
   $scope.message = 'Device Details';
   $scope.healthLog
   
   var deviceId = $routeParams.deviceId;
   

}]);

angular.module('devicesModule.controllers').controller('DeviceHealthLogController', 
    ['$scope', '$timeout', '$location', '$routeParams', 'deviceService', 
     function($scope, $timeout, $location, $routeParams, deviceService){
      
   var deviceId = $routeParams.deviceId;

   $scope.cpu = [];
   $scope.temp = [];
   $scope.ram = [];
   $scope.disk = [];

    /* Chart options */
   $scope.options = {
            chart: {
                type: 'stackedAreaChart',
                height: 300,
                margin : {
                    top: 40,
                    right: 40,
                    bottom: 60,
                    left: 40
                },
                x: function(d){return d[0];},
                y: function(d){return d[1];},
                useVoronoi: false,
                clipEdge: true,
                showControls: false,
                transitionDuration: 500,
                useInteractiveGuideline: true,
                xAxis: {
                    showMaxMin: false,
                    tickFormat: function(d) {
                        return d;
                    }
                },
                yAxis: {
                    tickFormat: function(d){
                        return d3.format(',.2f')(d);
                    }
                }
            }
        };


   var promise = deviceService.getDeviceHealthLogs(deviceId);   
     promise.then(function (data) {

         var cpuValues = [];
         var tempData = [];
         var memoryData = [];
         var diskData = [];
         var index = 0; 

          /* Chart data population */ 
         data.forEach(function(entry) {
              index++;

              var element = [index, entry.cpu];
              cpuValues.push(element);

              element = [index, entry.temperature];
              tempData.push(element);

              element = [index, ((entry.usedMemoryMB / entry.totalMemoryMB) * 100)];
              memoryData.push(element);

              element = [index, ((entry.usedDiskMB / entry.totalDiskMB) * 100)];
              diskData.push(element);

         });
          
          /* Chart data */
         var cpu_data = { 
               key: "CPU",
               color: "#51A351",
               values: cpuValues
           };

         var temp_data = { 
               key: "Temperature",
               values: tempData
           };

         var used_memory_data = { 
               key: "Used RAM",
               values: memoryData
           };

         var used_disk_data = { 
               key: "Used Disk MB",
               values: diskData
           };

           $scope.cpu.push(cpu_data);
           $scope.temp.push(temp_data);
           $scope.ram.push(used_memory_data);
           $scope.disk.push(used_disk_data);

           $scope.apiCpu.refresh();
           $scope.apiTemp.refresh();
           $scope.apiRam.refresh();
           $scope.apiDisk.refresh();
       },
      function (errorMessage) {
        // $scope.message = errorMessage + " ERROR";
     });
   
}]);

angular.module('devicesModule.controllers').controller('DeviceEventLogController', 
    ['$scope', '$timeout', '$location', '$routeParams', 'deviceService', 
     function($scope, $timeout, $location, $routeParams, deviceService){
      
   $scope.eventLogs = [];

   var deviceId = $routeParams.deviceId;

   var promise = deviceService.getDeviceEventLogs(deviceId);   
   promise.then(function (data) {
      
        var index = 0; 

          /* Chart data population */ 
         data.forEach(function(entry) {
              index++;
          
           var row = {
             logTime: entry.logTime,
             campaign: entry.campaignId,
             asset: entry.assetId,
             schedule: entry.assetScheduleId,
             duration: entry.duration,
             message: entry.message
           }

            $scope.eventLogs.push(row);
          });   

       $scope.eventLog = data;
     },
    function (errorMessage) {
      // $scope.message = errorMessage + " ERROR";
   });
   

}]);

angular.module('devicesModule.controllers').controller('DeviceRegisterController', 
  ['$scope', '$timeout', '$location', '$routeParams', 'deviceService', function($scope, $timeout, $location, $routeParams, deviceService)  {

   $scope.message = 'Device Registration';
   $scope.device = {};
   $scope.addButtonText ="Register Device";

    var deviceId = $routeParams.deviceId;
    if(deviceId) {
       var promise = deviceService.getById(deviceId); 

        promise.then(function (data) {
            $scope.device = data;
           },
           function (errorMessage) {
             $scope.message = errorMessage + " ERROR";
           }); 

        $scope.message = "Device Info";
        $scope.addButtonText ="Modify Device";
    }

    $scope.registerDevice = function() {
       $scope.addButtonText = "Saving. . .";

      var promise = deviceService.registerDevice($scope.device);

         promise.then(function (data) {
             $scope.message = "Device Registered";
             $scope.addButtonText = "Saved";

             $timeout(function() {
                $location.path('/list');
             }, 2000); 
               
           },
           function (errorMessage) {
             $scope.message = errorMessage + " ERROR";
           });
    }

}]);