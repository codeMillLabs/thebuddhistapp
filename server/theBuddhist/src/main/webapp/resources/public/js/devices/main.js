'use strict'

angular.module('devicesModule', ['ngRoute', 'devicesModule.controllers']);


// Define routes for devices module
angular.module('devicesModule').config(['$routeProvider', function($routeProvider) {
     
     $routeProvider.when('/list', {
     	templateUrl: 'partials/devices/device-list.html'
     }).
     when('/view/:deviceId', {
     	templateUrl: 'partials/devices/device-view.html'
     }).
     when('/register', {
     	templateUrl: 'partials/devices/device-registration.html'
     }).
     otherwise({
     	redirectTo: '/list'
     });
}]);