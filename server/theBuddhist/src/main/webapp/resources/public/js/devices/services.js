///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Device Module Services
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
'use strict'

angular.module('deviceModule.services', [])

angular.module('deviceModule.services').service('deviceService', [ '$http', '$q', function($http, $q) {
    return {
       listAll: function() {
           var deferred = $q.defer();

           $http.get('../../device/list')
           .then(function(response) {
               if (response.status == 200) {
               	deferred.resolve(response.data);
               } else {
               	deferred.reject('Error retrieving list of devices');
               }

           });

           return deferred.promise;
       },

       getById: function(deviceId) {
           var deferred = $q.defer();

           $http.get('../../device/get',{
                 params : {
                 	id: deviceId
                 }
           })
           .then(function(response) {
               if (response.status == 200) {
               	deferred.resolve(response.data);
               } else {
               	deferred.reject('Error retrieving device [ Device Id: ' + deviceId + ']');
               }
           });
           return deferred.promise;
       },

       updateDeviceInfo:  function(dirtyDeviceInfo) {
	       	var deferred = $q.defer();

	        $http({ method: 'POST',
                    url: '../../device/modify',
                    data:  dirtyDeviceInfo,
                    headers: {
                    	"Content-Type": "application/json",
                    	"Accept": "text/plain, application/json"
                    }
	            })
	           .then(function(response) {
	              if (response.status == 200) {
	               	deferred.resolve(response.data);
	               } else {
	               	deferred.reject('Error updating device infomation');
	               }

	        });
           return deferred.promise;
       },

       registerDevice:  function(dirtyDeviceInfo) {
	       	var deferred = $q.defer();

	        $http({ method: 'POST',
                    url: '../../device/reg',
                    data:  dirtyDeviceInfo,
                    headers: {
                    	"Content-Type": "application/json",
                    	"Accept": "text/plain, application/json"
                    }
	            })
	           .then(function(response) {
	              if (response.status == 200) {
	               	deferred.resolve(response.data);
	               } else {
	               	deferred.reject('Error registering device');
	               }
	        });
           return deferred.promise;
       },

       unregisterDevice:  function(deviceMacAddress) {
	       	var deferred = $q.defer();

	         $http.get('../../device/list',{
                 params : {
                 	mac: deviceMacAddress
                 }
             })
	         .then(function(response) {
	              if (response.status == 200) {
	               	deferred.resolve(response.data);
	               } else {
	               	deferred.reject('Error un-registering device');
	               }
	         });
          return deferred.promise;
      },
      
      getDeviceHealthLogs: function(deviceId) {
    	  var deferred = $q.defer();

	         $http.get('../../device/health/' + deviceId)
	         .then(function(response) {
	              if (response.status == 200) {
	               	deferred.resolve(response.data);
	               } else {
	               	deferred.reject('Error loading device health logs device');
	               }
	         });
       return deferred.promise;
      },

       getDeviceEventLogs: function(deviceId) {
        var deferred = $q.defer();

           $http.get('../../device/eventLog/' + deviceId)
           .then(function(response) {
                if (response.status == 200) {
                  deferred.resolve(response.data);
                 } else {
                  deferred.reject('Error loading device event logs device');
                 }
           });
       return deferred.promise;
      }
       
       
       
    }
}]);