/*
 * Copyright 2015 Amila Silva (amilasilva88@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codemill.buddhist.app.console;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.codemill.buddhist.app.console.config.root.ElasticSearchConfig;
import com.codemill.buddhist.app.console.config.root.RootContextConfig;
import com.codemill.buddhist.app.console.config.servlet.ServletContextConfig;
import com.codemill.buddhist.app.console.model.Content;
import com.codemill.buddhist.app.console.services.ContentService;

/**
 * @author Amila Silva
 *
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ElasticSearchConfig.class,
		RootContextConfig.class, ServletContextConfig.class })
public class ContentControllerTest {

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private ContentService contentService;

	private MockMvc mockMvc;

	@Before
	public void init() {
		mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testAddContent() throws Exception {
		mockMvc.perform(
				post("/content/add")
						.contentType(MediaType.APPLICATION_JSON)
						.content(
								"{\"title\": \"Dharma Chapter 1\", \"description\": \"Dharma Chapter 1 description\","
										+ "\"category\": \"Dharma\",  \"author\": \"A. Thero\","
										+ "\"publishedBy\": \"YMBA\", \"publishedDate\": "
										+ System.currentTimeMillis()
										+ ","
										+ "\"thumbUrl\": \"thumbUrl.html\",\"sourceUrl\": \"dharma chapter1.mp3\"}")
						.accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk());

		Page<Content> contentsPage = contentService.findAll(new PageRequest(0,
				10));

		assertNotNull("Content Page", contentsPage);
		assertFalse("Contents should not be empty", contentsPage.getContent()
				.isEmpty());
	}

	@Test
	public void testModifyContent() throws Exception {
		mockMvc.perform(
				post("/content/add")
						.contentType(MediaType.APPLICATION_JSON)
						.content(
								"{\"title\": \"Dharma Chapter 2\", \"description\": \"Dharma Chapter 2 description\","
										+ "\"category\": \"Dharma\",  \"author\": \"A. Thero\","
										+ "\"publishedBy\": \"YMBA- Borella\", \"publishedDate\": "
										+ System.currentTimeMillis()
										+ ","
										+ "\"thumbUrl\": \"thumbUrl-1.html\",\"sourceUrl\": \"dharma chapter1.mp3\"}")
						.accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk());

		Page<Content> contentsPage = contentService.findAll(new PageRequest(0,
				10));

		Content content = contentsPage.getContent().get(0);

		mockMvc.perform(
				post("/content/modify")
						.contentType(MediaType.APPLICATION_JSON)
						.content(
								"{\"id\": "
										+ content.getId()
										+ ", \"title\": \"Dharma Chapter 3\", \"description\": \"Dharma Chapter 3 description\","
										+ "\"category\": \"Dharma\",  \"author\": \"A. Thero\","
										+ "\"publishedBy\": \"YMBA\", \"publishedDate\": "
										+ System.currentTimeMillis()
										+ ","
										+ "\"thumbUrl\": \"thumbUrl-3.html\",\"sourceUrl\": \"dharma chapter3.mp3\"}")
						.accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk());

		contentsPage = contentService.findByTitleOrDescription(
				"Dharma Chapter 3", "", new PageRequest(0, 10));

		assertNotNull("Content Page", contentsPage);
		assertFalse("Contents should not be empty", contentsPage.getContent()
				.isEmpty());
		assertEquals("Dharma Chapter 3", contentsPage.getContent().get(0)
				.getTitle());
	}

	// @Test
	// public void testFindByFacet() throws Exception {
	// mockMvc.perform(
	// get("/content/findByFacet/category")
	// .contentType(MediaType.APPLICATION_JSON)
	// .accept(MediaType.APPLICATION_JSON)).andDo(print())
	// .andExpect(status().isOk());
	//
	// Page<Content> contentsPage = contentService.findAll(new PageRequest(0,
	// 10));
	//
	// Content content = contentsPage.getContent().get(0);
	//
	// mockMvc.perform(
	// post("/content/modify")
	// .contentType(MediaType.APPLICATION_JSON)
	// .content(
	// "{\"id\": "
	// + content.getId()
	// +
	// ", \"title\": \"Dharma Chapter 3\", \"description\": \"Dharma Chapter 3 description\","
	// + "\"category\": \"Dharma\",  \"author\": \"A. Thero\","
	// + "\"publishedBy\": \"YMBA\", \"publishedDate\": "
	// + System.currentTimeMillis()
	// + ","
	// +
	// "\"thumbUrl\": \"thumbUrl-3.html\",\"sourceUrl\": \"dharma chapter3.mp3\"}")
	// .accept(MediaType.APPLICATION_JSON)).andDo(print())
	// .andExpect(status().isOk());
	//
	// contentsPage = contentService.findByTitleOrDescription(
	// "Dharma Chapter 3", "", new PageRequest(0, 10));
	//
	// assertNotNull("Content Page", contentsPage);
	// assertFalse("Contents should not be empty", contentsPage.getContent()
	// .isEmpty());
	// assertEquals("Dharma Chapter 3", contentsPage.getContent().get(0)
	// .getTitle());
	// }

}
