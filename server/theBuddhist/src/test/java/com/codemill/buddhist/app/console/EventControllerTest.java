/*
 * Copyright 2015 Amila Silva (amilasilva88@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codemill.buddhist.app.console;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.codemill.buddhist.app.console.config.root.ElasticSearchConfig;
import com.codemill.buddhist.app.console.config.root.RootContextConfig;
import com.codemill.buddhist.app.console.config.servlet.ServletContextConfig;
import com.codemill.buddhist.app.console.model.Event;
import com.codemill.buddhist.app.console.services.EventService;

/**
 * @author Amila Silva
 *
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ElasticSearchConfig.class,
		RootContextConfig.class, ServletContextConfig.class })
public class EventControllerTest {
	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private EventService eventService;

	private MockMvc mockMvc;

	@Before
	public void init() {
		mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testAddContent() throws Exception {
		mockMvc.perform(
				post("/event/add")
						.contentType(MediaType.APPLICATION_JSON)
						.content(
								"{\"eventName\": \"Blood donation\", \"description\": \"Blood donation @ Dehiwela\","
										+ "\"location\": \"Dehiwela\",  \"country\": \"Sri Lanka\","
										+ "\"organizedBy\": \"JJK\", \"eventScheduleTime\": "
										+ System.currentTimeMillis()
										+ ","
										+ "\"priorityLevel\": 10}")
						.accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk());

		Page<Event> eventsPage = eventService.findAll(new PageRequest(0, 10));

		assertNotNull("Event Page", eventsPage);
		assertFalse("Events should not be empty", eventsPage.getContent()
				.isEmpty());
	}

	@Test
	public void testModifyContent() throws Exception {
		mockMvc.perform(
				post("/event/add")
						.contentType(MediaType.APPLICATION_JSON)
						.content(
								"{\"eventName\": \"Blood donation\", \"description\": \"Blood donation @ Dehiwela\","
										+ "\"location\": \"Dehiwela\",  \"country\": \"Sri Lanka\","
										+ "\"organizedBy\": \"JJK\", \"eventScheduleTime\": "
										+ System.currentTimeMillis()
										+ ","
										+ "\"priorityLevel\": 10}")
						.accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk());

		Page<Event> eventsPage = eventService.findAll(new PageRequest(0, 10));

		Event event = eventsPage.getContent().get(0);

		mockMvc.perform(
				post("/event/modify")
						.contentType(MediaType.APPLICATION_JSON)
						.content(
								"{\"id\": "
										+ event.getId()
										+ ", \"eventName\": \"Blood donation\", \"description\": \"Blood donation @ Kottawa\","
										+ "\"location\": \"Kottawa\",  \"country\": \"Sri Lanka\","
										+ "\"organizedBy\": \"JJK\", \"eventScheduleTime\": "
										+ System.currentTimeMillis() + ","
										+ "\"priorityLevel\": 10}")
						.accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk());

		eventsPage = eventService.findByString("Blood donation",
				new PageRequest(0, 10));

		assertNotNull("events Page", eventsPage);
		assertFalse("Events should not be empty", eventsPage.getContent()
				.isEmpty());
		assertEquals("Blood donation", eventsPage.getContent().get(0)
				.getEventName());
		assertEquals("Blood donation @ Kottawa", eventsPage.getContent().get(0)
				.getDescription());
	}

}
