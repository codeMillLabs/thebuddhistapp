/*
 * Copyright 2015 Amila Silva (amilasilva88@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codemill.buddhist.app.console;

import org.apache.commons.lang3.NotImplementedException;
import org.junit.Ignore;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * Utility methods used for test purposes
 *
 */
@Ignore
public class TestUtils {

	private TestUtils() {
		throw new NotImplementedException(
				"Utility classes cannot be instantiated");
	}

	public static Date date(int year, int month, int day) {
		return new Date(year - 1900, month - 1, day);
	}

	public static Time time(String timeStr) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		Time time = null;
		try {
			Date date = formatter.parse("1970/01/01 " + timeStr);
			time = new Time(date.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return time;
	}

}
